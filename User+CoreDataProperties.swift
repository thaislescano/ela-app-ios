//
//  User+CoreDataProperties.swift
//  ela-app
//
//  Created by Student on 2017-03-30.
//  Copyright © 2017 Student. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var user_name: String
    @NSManaged public var user_password: String
    @NSManaged public var firstDay_lastPeriod: Date
    @NSManaged public var lastDay_lastPeriod: Date
    @NSManaged public var cycle_length: Int64

}
