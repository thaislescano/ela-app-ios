//
//  ViewController.swift
//  ela-app
//
//  Created by Student on 2017-03-30.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
  
  
  @IBOutlet weak var imageLogo: UIImageView!
  @IBOutlet weak var user_password: UITextField!
  
  var managedObjectContext: NSManagedObjectContext!
  var user: User?
  
//  var loginOk = true
  
  @IBAction func Login(_ sender: Any) {
    
    
    let password = user_password.text
    
    
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"User")
    fetchRequest.predicate = NSPredicate(format: "user_password == %@", password!)
    print ("fetch \(fetchRequest)")
    
    if (user_password.text == "") {
      let alert = UIAlertController(title: "Oh, no",
                                    message: "You need a password to see your information:)",
                                    preferredStyle: .alert)
      let action = UIAlertAction(title: "Ok", style: .default,
                                 handler: nil)
      alert.addAction(action)
      present(alert, animated: true, completion: nil)
    }
        else if (user_password.text != "") {
      
          if (try? managedObjectContext.fetch(fetchRequest) as! [User]) != nil { // maybe here
//            performSegue(withIdentifier: "loginOk", sender: sender)
           
            
          }
          if (user_password.text == "admin") {
            performSegue(withIdentifier: "loginOk", sender: sender)
        
          } else {
            //handle error
            let alert = UIAlertController(title: "Oh, no",
                                      message: "Wrong password. Try again or register :)",
                                      preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default,
                                       handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }
  }
  
  @IBAction func register(_ sender: Any) {
   
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "registerUser" {
      let navigationController = segue.destination as! UINavigationController
      let controller = navigationController.topViewController as! RegisterTableViewController      
      controller.managedObjectContext = managedObjectContext
    }
    
    if segue.identifier == "loginOk" {
      let navigationController = segue.destination as! UINavigationController
      let controller = navigationController.topViewController as! UserInfoViewController
      controller.user = user
    }

  }
  
  
}
