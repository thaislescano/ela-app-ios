//
//  RegisterTableViewController.swift
//  ela-app
//
//  Created by Student on 2017-04-01.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import CoreData

class RegisterTableViewController: UITableViewController {
  
  var managedObjectContext: NSManagedObjectContext!
  
  private var dpShowDateVisible = false

  @IBOutlet weak var new_user_password: UITextField!
  @IBOutlet weak var new_user_name: UITextField!

  @IBOutlet weak var new_user_cyclelength: UITextField!

  
  @IBOutlet weak var label_first_dayRegister: UILabel!
  @IBOutlet weak var label_last_dayRegister: UILabel!
  
  @IBOutlet weak var date_picker2: UIDatePicker!
  @IBOutlet weak var date_picker: UIDatePicker!
  
  override func viewDidLoad() {
    
    
    super.viewDidLoad()
    
    date_picker.addTarget(self, action: #selector(RegisterTableViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
    date_picker2.addTarget(self, action: #selector(RegisterTableViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
    
  }
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  //-----date picker --//
  
  func datePickerValueChanged (datePicker: UIDatePicker) {
  
    let dateformatter = DateFormatter()
    
    dateformatter.dateStyle = DateFormatter.Style.short
    
    
    let dateValue_first = dateformatter.string(from: date_picker2.date)
    let dateValue_second = dateformatter.string(from: date_picker.date)
    
    label_first_dayRegister.text = dateValue_first
    label_last_dayRegister.text = dateValue_second

  
  }
  private func toggleShowDateDatepicker () {
    dpShowDateVisible = !dpShowDateVisible
    
    tableView.beginUpdates()
    tableView.endUpdates()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 2 || indexPath.row == 4 {
      toggleShowDateDatepicker()
    }
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if !dpShowDateVisible && indexPath.row == 3 {
      return 0
    }
    if !dpShowDateVisible && indexPath.row == 5{
      return 0
    }
    else {
      return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
  }
  
  
  //----------------------------//
  @IBAction func registerUser(_ sender: Any) {
    
    let user = User(context: managedObjectContext)
    
    user.user_name = new_user_name.text!
    user.user_password = new_user_password.text!
    user.firstDay_lastPeriod = self.date_picker.date
    user.lastDay_lastPeriod = self.date_picker2.date
    user.cycle_length = Int64 (new_user_cyclelength.text!)!
    
    
    do {
      try managedObjectContext.save()
      print("Saved :)")
    } catch {
      fatalError("Failure to save context: \(error)")
    }

    
  }
  
  
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }

  @IBAction func cancel(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }


}
